﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    private Rigidbody _rb;
    public int speed;

	// Use this for initialization
	void Start () {
        _rb = GetComponent<Rigidbody>();
	}

    // Update is called once per frame
    //void Update () {
    //
    //}

    private void FixedUpdate()
    {
        float moveVertical = Input.GetAxis("Vertical");
        float moveHorizontal = Input.GetAxis("Horizontal");

        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

        _rb.AddForce(movement * speed);
    }
}
